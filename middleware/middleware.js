const jwt = require('jsonwebtoken');
const config = require('../config/config');

let middleware = {

  authenticateUser: function(req, res, next) {
    var token = req.query.jwtToken;
    if (token) {
      jwt.verify(token, config.jwtPassword, (error, data) => {
        if (error) {
          return res.redirect('/login', 204, {message: 'Please log in again'});
        } else {
          req.data = data;
          next();
        }
      });
    } else {
      return res.redirect('/login', 204, {message: 'Not token given.'});
    }
  }
}

module.exports = middleware;
